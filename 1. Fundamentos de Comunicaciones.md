# 1. Fundamentos de Comunicaciones

## Elementos de un Sistema de Comunicaciones (Tomasi, 2003)

- El objetivo fundamental de un *sistema de comunicaciones*, es transferir *información* de un lugar a otro.
- "Las comunicaciones electrónicas son la *transmisión, recepción y procesamiento* de información entre dos o más lugares, mediante circuitos electrónicos" (Tomasi, 2003).
- La fuente original de información puede estar en forma analógica (continua), como por ejemplo la voz humana o la música, o en forma digital (discreta), como por ejemplo los números codificados binariamente o los códigos alfanuméricos.
- Todas las formas de información se deben convertir a energía electromagnética antes de ser propagadas a través de un sistema electrónico de comunicaciones
- Samuel Morse desarrolló en 1837 el primer sistema electrónico de comunicaciones. Usó la inducción electromagnética para transferir información en forma de puntos, rayas y espacios entre un transmisor y un receptor sencillos, usando una línea de transmisión que consistía en un tramo de conductor metálico. Llamó telégrafo a su invento.
- En 1876, Alexander Graham Bell y Thomas A. Watson fueron los primeros en transferir en forma exitosa la conversación humana a través de un sistema sencillo de comunicaciones con hilo metálico, al que llamaron teléfono.
- Guglielmo Marconi transmitió por primera vez señales de radio, sin hilos, a través de la atmósfera terrestre, en 1894.
- Lee DeForest inventó en 1908 el triodo, o válvula al vacío, que permitió contar con el primer método práctico para amplificar las señales eléctricas. La radio comercial comenzó en 1920, cuando las estaciones de radio comenzaron a emitir señales de amplitud modulada (AM), y en 1933 el mayor Edwin Howard Armstrong inventó la modulación de frecuencia (FM). La emisión comercial en FM comenzó en 1936.
- Aunque los conceptos y principios fundamentales de las comunicaciones electrónicas han cambiado poco desde su introducción, los métodos y circuitos con que se realizan han sufrido grandes cambios.
- Los transistores y los circuitos integrados lineales han
simplificado el diseño de los circuitos de comunicación electrónica, permitiendo así la miniaturización, mejor eficiencia y confiabilidad y costos generales menores.
- En los años recientes ha habido una necesidad abrumadora de comunicación entre cada vez más personas que ha estimulado un crecimiento gigantesco de la industria de comunicaciones electrónicas.
- Los sistemas electrónicos modernos de comunicación incluyen los de cable metálico, por microondas y los satelitales, así como los sistemas de fibra óptica.

## Cuestiones básicas (Stallings, 2004)

- Todos los tipos de información considerados en este texto (voz, datos, imágenes, vídeo) se pueden representar mediante señales electromagnéticas. Para transportar la información, dependiendo del medio de transmisión y del entorno donde se realicen las comunicaciones, se podrán utilizar señales analógicas o digitales.
- Cualquier señal electromagnética, analógica o digital, está formada por una serie de frecuencias constituyentes. Un parámetro clave en la caracterización de la señal es el ancho de banda, definido como el rango de frecuencias contenidas en la señal. En términos generales, cuanto mayor es el ancho de banda de la señal, mayor es su capacidad de transportar información.
- Uno de los problemas principales en el diseño de un sistema de comunicaciones reside en paliar las dificultades, o defectos, de las líneas de transmisión. 
- Las dificultades más importantes a superar son la atenuación, la distorsión de atenuación, la distorsión de retardo, así como los distintos tipos de ruido. 
- El ruido puede ser, entre otros, de tipo térmico, ruido de intermodulación, diafonía o impulsivo. 
- Al usar señales analógicas, las dificultades en la transmisión causan efectos de naturaleza aleatoria que degradan la calidad de la información recibida y pueden afectar a la inteligibilidad. 
- Cuando se utilizan señales digitales, los defectos en la transmisión pueden introducir bits erróneos en la recepción.
- El diseñador de un sistema de comunicaciones debe tener presente cuatro factores determinantes:
  - el ancho de banda de la señal, 
  - la velocidad de transmisión de la información digital, 
  - la cantidad de ruido, 
  - además de otros defectos en la transmisión,
  - la proporción o tasa de errores tolerable. 
- El ancho de banda disponible está limitado por el medio de transmisión así como por la necesidad de evitar interferencias con señales cercanas. 
- Debido a que el ancho de banda es un recurso escaso, es conveniente hacer máxima la velocidad de transmisión para el ancho de banda disponible. 
- La velocidad de transmisión está limitada por el ancho de banda, por la presencia ineludible de defectos en la transmisión, como el ruido, y, finalmente, por la tasa de errores que sea tolerable.

## Terminología utilizada

- La transmisión de datos entre un emisor y un receptor siempre se realiza a través de un medio de transmisión. 
- Los medios de transmisión se pueden clasificar como **guiados** y **no guiados**. En ambos casos, la comunicación se realiza usando ondas electromagnéticas. 
- En los medios guiados, por ejemplo en pares trenzados, en cables coaxiales y en fibras ópticas, las ondas se transmiten confinándolas a lo largo de un camino físico. 
- Por el contrario, los medios no guiados, también denominados inalámbricos, proporcionan un medio para transmitir las ondas electromagnéticas sin confinarlas, como por ejemplo en la propagación a través del aire, el mar o el vacío.
- El término enlace directo se usa para designar un camino de transmisión entre dos dispositivos en el que la señal se propague directamente del emisor al receptor sin ningún otro dispositivo intermedio que no sea un amplificador o repetidor. Estos últimos se usan para incrementar la energía de la señal. Obsérvese que este término se puede aplicar tanto a medios guiados como no guiados.
- Un medio de transmisión guiado es punto a punto si proporciona un enlace directo entre dos dispositivos que comparten el medio, no existiendo ningún otro dispositivo conectado. En una configuración guiada multipunto, el mismo medio es compartido por más de dos dispositivos.

## Sistemas Electrónicos de Comunicaciones (Tomasi, 2003)
  
  ![Sistema de comunicaciones](images/Sistema_comunicaciones.jpg)
- La fig. 1-1 muestra un diagrama de bloques simplificado de un sistema electrónico de comunicaciones, que comprende un transmisor, un medio de transmisión y un receptor. 
- Un transmisor es un conjunto de uno o más dispositivos o circuitos electrónicos que convierte la información de la fuente original en una señal que se presta más a su transmisión a través de determinado medio de transmisión. 
- El medio de transmisión transporta las señales desde el transmisor hasta el receptor, y puede ser tan sencillo como un par de conductores de cobre que propaguen las señales en forma de flujo de corriente eléctrica. 
- También se puede convertir la información a ondas electromagnéticas luminosas, propagarlas a través de cables de fibra óptica hechas de vidrio o de plástico, o bien se puede usar el espacio libre para transmitir ondas electromagnéticas de radio, a grandes distancias o sobre terreno donde sea difícil o costoso instalar un cable físico. 
- Un receptor es un conjunto de dispositivos y circuitos electrónicos que acepta del medio de transmisión las señales transmitidas y las reconvierte a su forma original.

## Modos de Transmisión (Tomasi, 2003)

Los cuatro modos de transmisión son:
- **Simplex (SX):** Las transmisiones sólo se hacen en una dirección, solo recibir o solo transmitir. Una estación puede ser un transmisor o un receptor. Ej: Radio, TV.
- **Semidúplex (HDX, Half duplex):** Las transmisiones se pueden hacer en ambas direcciones, pero no al mismo tiempo, también se les llama de cambio y fuera. Una estación puede ser transmisora y receptora pero no al mismo tiempo. Los sistemas de radio en dos sentidos con botones para conectar (PTT, push-to-talk) sus transmisores, como los radios de banda civil y policía.
- **Duplex total (FDX, Full Duplex):** Se puede hacer transmisiones en ambas direcciones al mismo tiempo. Una estación puede transmitir y recibir en forma simultánea; sin embargo, la estación a la que se le transmite también debe ser de la que recibe. Ej: Sistema telefónico.
- **Duplex total/general (F/FDX, de full/full duplex):** Transmitir de forma simultánea, pero no necesariamente entre las mismas dos estaciones (una estación puede transmitir a una segunda estación, y recibir al mismo tiempo de una tercera estación). Ej: Circuitos de comunicaciones de datos, servicio postal.

## Configuraciones de los circuitos

### Transmisión a dos hilos

- La transmisión a dos hilos usa dos conductores, uno para señal y otro para referencia, o tierra.

### Transmisión a cuatro hilos

- La transmisión a cuatro hilos usa cuatro conductores, dos en cada dirección.
- Ventaja que ingresa menos ruido y proporciona más aislamiento entre las dos direcciones de transmisión.

### Híbridos y supresores de eco

- El conjunto híbrido se usa para compensar impedancias y proporcionar aislamiento entre las dos direcciones de flujo de la señal.
- El supresor de eco consiste en un amplificador en la dirección de la comunicación, de tal forma que no regrese la señal de salida hacia la entrada, en el caso de algún desbalance en las impedancias.
- AT&T pone supresores de eco cada 1500 millas.

## Transmisión de datos

- El éxito en la transmisión de datos depende fundamentalmente de dos factores: 
  - la calidad de la señal que se transmite y 
  - las características del medio de transmisión.

## Análisis de Señales

- Aunque las señales de las comunicaciones electrónicas no son senoidales de una sola frecuencia, muchas de ellas si lo son, y las que no, se las puede representar con una combinación de funciones seno y coseno.

### Señales periódicas

$$x(t)=x(t\pm T)$$

### Señales senoidales

- Análisis de Señales: Análisis de frecuencia, longitud de onda, Amplitud de voltaje de la señal.

  $$v(t)=Vcos(2\pi ft + \theta)$$

  $$sen(\theta)=cos(\theta - \frac{\pi}{2})$$

- **Dominio del tiempo:** Osciloscopio, determina la *forma de onda de la señal*, que consiste en la forma y la magnitud instantánea de la señal respecto al tiempo.

  ![Señal en el tiempo](images/senal_tiempo.jpg)

- **Dominio de la frecuencia:** El analizador de espectro es un instrumento del dominio de la frecuencia. No se muestra ninguna forma de onda, sino una gráfica de amplitud vs frecuencia, conocida como *espectro de frecuencia*.

  ![Espectro de frecuencia](images/escpectro_frecuencia.jpg)

### Ondas periódicas no senoidales (Ondas complejas)

- Toda onda repetitiva formada por más de una onda senoidal o cosenoidal relacionada armónicamente, es una onda no senoidal u onda periódica compleja.

#### La serie de Fourier

- Serie que usa el análisis de señales para representar las componentes senoidales de una onda periódica no senoidal, es decir, para cambiar una señal del dominio del tiempo al dominio de la frecuencia.

  $$f(t)=A_0+\sum_{k=1}^{\infty}A_k\cos(kw_0t)+\sum_{k=1}^{\infty}B_k\sin(kw_0t) \\ w_o=2\pi f$$

![Suma de Componentes](images/suma_de_componentes.jpg)

![Dominio de la frecuencia](images/dominio_frecuencia.jpg)

- Se define **espectro** de una señal como el conjunto de frecuencias que la constituyen.
- Se define el **ancho de banda absoluto** de una señal como la anchura del espectro. Ej: Dos frecuencias $f$ y $3f$, el ancho de banda es $2f$.
- Las señales como la señal de 3.5b tiene un ancho de banda infinito, sin embargo, la mayor cantidad de energía de la señal se concentra en una banda de frecuencias relativamente estrecha, lo que se denomina **ancho de banda efectivo**, o simplemente **ancho de banda**.
- La **componente continua** se refiere a la señal con componente de frecuencia cero.

## Modulación y Demodulación

- Como a menudo no es práctico propagar señales de información a través de cables metálicos o de fibra óptica, o a través de la atmósfera terrestre, con frecuencia es necesario modular la información de la fuente, con una señal analógica de mayor frecuencia, llamada *portadora*.
- la señal portadora transporta la información a través del sistema. La señal de información modula a la portadora, cambiando su amplitud, su frecuencia o su fase. 
- "Modulación no es más que el proceso de cambiar una o más propiedades de la portadora, en proporción con la señal de información" (Tomasi, 2003).
- Los dos tipos básicos de comunicaciones electrónicas son analógico y digital. 
- Un sistema analógico de comunicaciones es aquel en el cual la energía se transmite y se recibe en forma analógica: una señal de variación continua, como por ejemplo una onda senoidal. 
- En los sistemas analógicos de comunicaciones, tanto la información como la portadora son señales analógicas.
- El término comunicaciones digitales abarca una amplia variedad de técnicas de comunicación, que incluyen transmisión digital y radio digital. 
- La transmisión digital es un sistema digital verdadero, donde los pulsos digitales (con valores discretos, como +5V y tierra) se transfieren entre dos o más puntos en un sistema de comunicaciones. 
- Con la transmisión digital no hay portadora analógica, y la fuente original de información puede tener forma digital o analógica. Si está en forma analógica se debe convertir a pulsos digitales antes de transmitirla, se debe reconvertir a la forma analógica en el extremo de recepción. 
- Los sistemas de transmisión digital requieren una instalación física entre el transmisor y el receptor, como por ejemplo un conductor metálico o un cable de fibra óptica.
- La radio digital es la transmisión de portadoras analógicas moduladas digitalmente, entre dos o más puntos en un sistema de comunicaciones. 
- En la radio digital, la señal moduladora y la señal demodulada son pulsos digitales. Estos pulsos se pueden originar en un sistema digital de transmisión, en una fuente digital, como por ejemplo una computadora, o pueden ser una señal analógica codificada en binario. 
En los sistemas digitales de radio, el medio de transmisión puede ser una instalación física o el espacio libre (es decir, la atmósfera terrestre). 
- Los sistemas analógicos de comunicaciones fueron los primeros en ser desarrollados; sin embargo, en tiempos recientes, se han popularizado más los sistemas digitales de comunicaciones.
  $$v(t) = V sin(2\pi ft+\theta)$$
- La ecuación 1-1 es la descripción general de una onda senoidal de voltaje, variable en el tiempo, como puede ser una señal portadora de alta frecuencia. 
- Si la señal de información es analógica, y la amplitud (V) de la portadora es proporcional a ella, se produce la modulación de amplitud (AM, por amplitude modulation). 
- Si se varía la frecuencia ( f ) en forma proporcional
a la señal de información, se produce la modulación de frecuencia (FM, de frequency modulation); 
- por último, si se varía la fase ($\theta$) en proporción con la señal de información, se produce la modulación de fase (PM, de phase modulation).
- Si la señal de información es digital, y la amplitud (V) de la portadora se varía proporcionalmente a la señal de información, se produce una señal modulada digitalmente, llamada modulación por conmutación de amplitud (ASK, de amplitude shift keying). 
- Si la frecuencia ( f ) varía en forma proporcional a la señal de información se produce la modulación por conmutación de frecuencia (FSK, de frequency shift keying), - y si la fase (␪) varía de manera proporcional a la señal de información, se produce la modulación por conmutación de fase (PSK, de phase shift keying). 
- Si se varían al mismo tiempo la amplitud y la fase en proporción con la señal de información, resulta la modulación de amplitud en cuadratura (QAM, de quadrature amplitude modulation). 
- Los sistemas ASK, FSK, PSK y QAM son formas de modulación digital, y se describiráncon detalle en el capítulo 12 (más adelante).
- La modulación se hace en un transmisor mediante un circuito llamado modulador. 
- Una portadora sobre la que ha actuado una señal de información se llama *onda modulada o señal modulada*. 
- La demodulación es el proceso inverso a la modulación, y reconvierte a la portadora modulada en la información original (es decir, quita la información de la portadora). 
- La demodulación se hace en un receptor, con un circuito llamado demodulador.
- Hay dos razones por las que la modulación es necesaria en las comunicaciones electrónicas: 
  1. Es en extremo difícil irradiar señales de baja frecuencia en forma de energía electromagnética, con una antena, y 
  2. ocasionalmente, las señales de la información ocupan la misma banda de frecuencias y si se transmiten al mismo tiempo las señales de dos o más fuentes, interferirán entre sí. 
- Por ejemplo, todas las estaciones comerciales de FM emiten señales de voz y música que ocupan la banda de audiofrecuencias, desde unos 300 Hz hasta 15 kHz. Para evitar su interferencia mutua, cada estación convierte a su información a una banda o canal de frecuencia distinto. 
- Se suele usar el término canal para indicar determinada banda de frecuencias asignada a determinado servicio. 
- Un canal normal de banda de voz ocupa más o menos 3 kHz de ancho de banda, y se usa para transmitir señales como las de voz; los canales comerciales de emisión en AM ocupan una banda de frecuencias de 10 kHz, y en los canales de radio de microondas y vía satélite se requiere un ancho de banda de 30 MHz o más.

  ![Sistema de Comunicaciones con Modulador](images/Sistema_comunicaciones_modulador.jpg)
- La fig. 1-2 es el diagrama simplificado de bloques de un sistema electrónico de comunicaciones, donde se ven las relaciones entre la señal moduladora, la portadora de alta frecuencia y la onda modulada.

## El Espectro Electromagnético (Tomasi, 2003)

- El objetivo de un sistema electrónico de comunicaciones es transferir información entre dos o más lugares, cuyo nombre común es estaciones. Esto se logra convirtiendo la información original a energía electromagnética, para transmitirla a continuación a una o más estaciones receptoras, donde se reconvierte a su forma original. 
- La energía electromagnética se puede propagar en forma de voltaje o corriente, a través de un conductor o hilo metálico, o bien en forma de ondas de radio emitidas hacia el espacio libre, o como ondas luminosas a través de una fibra óptica. 
- La energía electromagnética se distribuye en un intervalo casi infinito de frecuencias.
- La frecuencia no es más que la cantidad de veces que sucede un movimiento periódico, como puede ser una onda senoidal de voltaje o de corriente, durante determinado periodo. 
- Cada inversión completa de la onda se llama ciclo. La unidad básica de frecuencia es el hertz (Hz), y un hertz es igual a un ciclo por segundo (1 Hz = 1 cps). 
- En electrónica se acostumbra usar prefijos métricos para representar las grandes frecuencias. Por ejemplo, se usa el kHz (kilohertz) para indicar miles de hertz, y el MHz (megahertz) para indicar millones de hertz.

  ![Espectro Radioeléctrico](images/espectro.png)
  ![Espectro Radioeléctrico Tomasi](images/Espectro_RadioElectrico2.jpg)

### Frecuencias de Transmisión

- El espectro electromagnético de frecuencias total, donde se muestran los lugares aproximados de diversos servicios, se ve en la fig. 1-3. Este espectro de frecuencias va desde las subsónicas (unos pocos hertz) hasta los rayos cósmicos (1022 Hz).
- El espectro de frecuencias se subdivide en subsecciones o bandas. Cada banda tiene un nombre y sus límites. En los Estados Unidos, las asignaciones de frecuencias para radio propagación en el espacio libre son realizadas por la Comisión Federal de Comunicaciones (FCC).
- Por ejemplo, la banda de emisión comercial en FM tiene asignadas las frecuencias de 88 MHz a 108 MHz. 
- Las frecuencias exactas asignadas a transmisores específicos que funcionan en las diversas clases de servicio se actualizan y alteran en forma constante, para cumplir con las *necesidades de comunicaciones en una **nación***.
- El espectro total útil de radiofrecuencias (RF) se divide en bandas de frecuencia más angostas, a las que se dan nombres y números descriptivos, y algunas de ellas se subdividen a su vez en diversos tipos de servicios. Las designaciones de banda según el Comité consultivo internacional de radio (CCIR) se muestran en la tabla 1-1. Estas designaciones se resumen como sigue:
    - Frecuencias extremadamente bajas (ELF, de extremely low frequencies). Son señales en el intervalo de 30 a 300 Hz, y comprenden las señales de distribución eléctrica (60 Hz) y las de telemetría de baja frecuencia.
    - Frecuencias de voz (VF, de voice frequencies). Son señales en el intervalo de 300 a 3000 Hz, e incluyen a las que generalmente se asocian a la voz humana. Los canales telefónicos normales tienen un ancho de banda de 300 a 3000 Hz, y con frecuencia se llaman canales de frecuencia de voz, o canales de banda de voz.
    - Frecuencias muy bajas (VLF, de very low frequencies). Son señales dentro de los límites de 3 a 30 kHz, que comprenden al extremo superior del intervalo audible humano. Las VLF se usan en algunos sistemas especiales, del gobierno y militares, como por ejemplo las comunicaciones con submarinos.
    - Frecuencias bajas (LF, de low frequencies). Son señales en el intervalo de 30 a 300 kHz, y se usan principalmente en la navegación marina y aeronáutica.
    - Frecuencias intermedias (MF, de medium frequencies). Son señales de 300 kHz a 3 MHz, y se usan principalmente para emisiones comerciales de radio AM (535 a 1605 kHz).
    - Frecuencias altas (HF, de high frequencies). Señales en el intervalo de 3 a 30 MHz, con frecuencia llamadas ondas cortas. La mayoría de las radiocomunicaciones en dos sentidos usa este intervalo, y la Voz de América y la Radio Europa Libre transmiten en él. También los radio aficionados y la banda civil (CB) usan señales de HF.
    - Muy altas frecuencias (VHF, por very high frequencies). Son señales de 30 a 300 MHz, y se usan en radios móviles, comunicaciones marinas y aeronáuticas, emisión comercial en FM (de 88 a 108 MHz) y en la emisión de televisión, en los canales 2 a 13 (54 a 216 MHz).
    - Frecuencias ultra altas (UHF, de ultrahigh frequencies). Son señales entre los límites de 300 MHz a 3 GHz, y las usa la emisión comercial de televisión, en los canales 14 a 83, en los servicios móviles de comunicaciones terrestres, teléfonos celulares, algunos sistemas de radar y de navegación, y los sistemas de radio por microondas y por satélite. Hablando con generalidad, se considera que las frecuencias mayores que 1 GHz son de microondas, y eso incluye al extremo superior del intervalo de UHF.
    - Frecuencias super altas (SHF, por superhigh frequencies). Son señales de 3 a 30 GHz, donde está la mayoría de las frecuencias que se usan en sistemas de radiocomunicaciones por microondas y satelitales.
    - Frecuencias extremadamente altas (EHF, de extremely high frequencies). Son señales entre 30 y 300 GHz, y casi no se usan para radiocomunicaciones, a excepción de aplicaciones muy complicadas, costosas y especializadas.
    - Infrarrojo. Las frecuencias del infrarrojo son señales de 0.3 a 300 THz, y por lo general no se les considera como ondas de radio. Infrarrojo indica una radiación electromagnética que en general se asocia con el calor. Las señales infrarrojas se usan en sistemas de guía de proyectiles con blancos térmicos, o con la fotografía electrónica y la astronomía.
    - Luz visible. En la luz visible se incluyen las frecuencias electromagnéticas captadas por el ojo humano (0.3 a 3 PHz). Las comunicaciones con ondas luminosas se usan en los sistemas de fibra óptica, que en los últimos años han llegado a ser un medio principal de transmisión en los sistemas electrónicos de comunicaciones.
    - Rayos ultravioleta, rayos X, rayos gamma y rayos cósmicos: tienen poca aplicación en las comunicaciones electrónicas y en consecuencia no se describirán.

![Tabla Designaciones de banda CCIR](images/Tabla%20Frecuencias%20Espectro.jpg)

- Cuando se manejan ondas de radio se acostumbra usar unidades de longitud de onda, y no de frecuencia. 
- La longitud de onda es la distancia que ocupa en el espacio un ciclo de una onda electromagnética, es decir, la distancia entre los puntos correspondientes en una onda repetitiva. La longitud de onda es inversamente proporcional a la frecuencia de la onda, y directamente proporcional a su velocidad de propagación. Se supone que la velocidad de propagación de la energía electromagnética en el espacio libre es 3 ϫ 108 m/s. La relación entre frecuencia, velocidad y longitud de onda se expresa en forma matemática como sigue
$$ \lambda = \frac{c}{f}$$

![Espectro RadioElectrico Longitudes de onda](images/Espectro_RadioElectrico_Longitudes.jpg)

#### Ejercicio:

Calcular la longitud de onda para las frecuencias 1KHz, 100KHz, 10MHz

### Clasificación de los transmisores

- Para fines de registro en Estados Unidos, los radiotransmisores se clasifican según su ancho de banda, esquema de modulación y tipo de información. 
- Las  clasificaciones de emisión se identifican con una clave de tres símbolos, que contiene una combinación de letras y números, como se ve en la tabla 1-2. El primer símbolo es una letra que indica el tipo de modulación de la portadora principal. El segundo símbolo es un número que identifica al tipo de emisión, y el tercer símbolo es otra letra que describe el tipo de información que se transmite. 
- Por ejemplo, la designación A3E describe una señal por doble banda lateral, portadora completa, de amplitud modulada, que conduce información telefónica, de voz o de música.

![Tabla de transmisores](images/Tabla%20Transmisores.jpg)

## Ancho de Banda y Capacidad de Información

- Las dos limitaciones más importantes en el funcionamiento de un sistema de comunicaciones son el ruido y el ancho de banda. 
- El ancho de banda de una señal de información no es más que la diferencia entre las frecuencias máxima y mínima contenidas en la información, y el ancho de banda de un canal de comunicaciones es la diferencia entre las frecuencias máxima y mínima que pueden pasar por el canal (es decir, son su banda de paso). 
- El ancho de banda de un canal de comunicaciones debe ser suficientemente grande (ancho) para pasar todas las frecuencias importantes de la información. En otras palabras, el ancho de banda del canal de comunicaciones debe ser igual o mayor que el ancho de banda de la información. 
- Por ejemplo, las frecuencias de voz contienen señales de 300 a 3000 Hz. Por consiguiente, un canal para frecuencias de voz debe tener una amplitud igual o mayor que 2700 Hz. 
- Si un sistema de transmisión de televisión por cable tiene una banda de paso de 500 a 5000 kHz, su amplitud de banda es 4500 kHz. 
- Como regla general, un canal de comunicaciones no puede propagar una señal que contenga una frecuencia que cambie con
mayor rapidez que la amplitud de banda del canal.

### Teoría de la información

- La teoría de la información es el estudio muy profundo del uso eficiente del ancho de banda para propagar información a través de sistemas electrónicos de comunicaciones. 
- Esta teoría se puede usar para determinar la capacidad de información de un sistema de comunicaciones.
- La capacidad de información es una medida de cuánta información se puede transferir a través de un sistema de comunicaciones en determinado tiempo. 
- La cantidad de información que se puede propagar en un sistema de transmisión es una función del ancho de banda y del tiempo de transmisión. 
- R. Hartley, de los Bell Telephone Laboratories, desarrolló en 1920 la relación entre el ancho de banda, el tiempo de transmisión y la capacidad de información. La ley de Hartley sólo establece que mientras más amplio sea el ancho de banda y mayor sea el tiempo de transmisión, se podrá enviar más información a través del sistema. En forma matemática, la ley
de Hartley es
  
  $$I\propto B \text{ x } t$$

  Donde I = capacidad de información, B = Ancho de banda, t = tiempo de transmisión

- La ecuación 1-3 indica que la capacidad de información es una función lineal, y es directamente proporcional tanto al ancho de banda del sistema como al tiempo de transmisión. Si sube al doble el ancho de banda en un sistema de comunicaciones, también se duplica la cantidad de información que puede transportar. Si el tiempo de transmisión aumenta o disminuye, hay un cambio proporcional en la cantidad de información que el sistema puede transferir.
- En general, mientras más compleja sea la señal de información, se requiere más amplitud de banda para transportarla en determinado tiempo. 
- Se requieren unos 3 kHz de amplitud de banda para transmitir las señales telefónicas con calidad de voz. En contraste, se asignan 200 kHz de ancho de banda a la transmisión comercial de FM para música, con alta fidelidad, y se requieren casi 6 MHz de ancho de banda para emitir señales de televisión de alta calidad.
- C. E. Shannon (también de Bell Telephone Laboratories) publicó en 1948 un trabajo en el Bell System Technical Journal, donde relacionó la capacidad de información de un canal de comunicaciones, en bits por segundo (bps), con el ancho de banda y la relación de señal a ruido.
- La expresión matemática del límite de Shannon de capacidad de información es

$$I=B \log_2(1+\frac{S}{N})$$

Donde I = Capacidad de información (bits por segundo), B = Ancho de banda (Hertz), S/N = Relación de potencia de señal a ruido (sin unidades)

- Ejemplo: Para un canal normal de comunicaciones en banda de voz, con una relación de potencias de señal a ruido de 1000 (30 dB) y un ancho de banda de 2.7 kHz, el límite de Shannon de capacidad de información es
  $$I=2700\log_2(1+1000) \newline = 26.9Kbps$$

- Con frecuencia se entiende mal la fórmula de Shannon. Los resultados del ejemplo anterior indican que se pueden transferir 26.9 kbps a través de un canal de 2.7 kHz. Esto podría ser cierto, pero no se puede hacer en un sistema binario. 
- Para alcanzar una rapidez de transmisión de información de 26.9 kbps a través de un canal de 2.7 kHz, cada símbolo que se transfiera debe contener más de un bit de información. Por consiguiente, para llegar al límite de Shannon de capacidad de información, se deben usar sistemas digitales de transmisión que tengan más de dos condiciones (símbolos) de salida.

## Relación entre la velocidad de transmisión y el ancho de banda



