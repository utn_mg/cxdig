# Comunicación Digital

Contenido:

1. [Fundamentos de Comunicaciones](1.%20Fundamentos%20de%20Comunicaciones.md)
    - Elementos de un sistema de comunicación
    - Alteraciones de la señal: Interferencia, Distorsión, Ruido
    - Estandarización: Unidades de medida en Telecomunicaciones
    - Análisis de la señal: Dominio temporal, Dominio frecuencia, Series de Fourier, señales periódicas y no períodicas
    - Comunicación de Datos: Códigos, Modos de Tx: Serial, Paralela, Sincrónica, asincrónica, half duplex, full duplex
    - Parámetros de un canal de Tx: VTX, VS C, Eficiencia espectral, ISI, Jitter
2. Transmisión Digital en banda base

## Referencia Bibliográfica

- Wayne Tomasi (2003), Sistemas de Comunicaciones Electrónicas, Cuarta Edición, Prentice Hall.
- William Stallings (2004), Comunicaciones y Redes de Computadoras, Septima Edición, Pearson Prentice Hall
